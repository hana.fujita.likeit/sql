﻿USE practice;

SELECT i2.*, i1.category_name

FROM item_category i1

INNER JOIN
item i2 

ON
i2.category_name = i1.category_name

SUM item_price AS total_price
;
