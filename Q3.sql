﻿CREATE DATABASE Q3 DEFAULT CHARACTER SET utf8;
USE Q3;
CREATE TABLE item_category(
category_id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
category_name varchar(256) NOT NULL
);
INSERT INTO item_category(category_id, category_name) VALUES(1, '家具');
INSERT INTO item_category(category_id, category_name) VALUES(2, '食品');
INSERT INTO item_category(category_id, category_name) VALUES(3, '本');

--★ほんとは、データベースQ1を用いて、かつ新たにTABLE作らずに解かないといけないのでは？
--★NOT NULL等足すときは、,いらない！！あったらだめ！

